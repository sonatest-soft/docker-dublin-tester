FROM ubuntu:xenial
MAINTAINER Bruno Plamondon <plamondonb@sonatest.com>

ENV DEBIAN_FRONTEND noninteractive

# install tools for compiling projects
RUN apt-get update && apt-get install -y --no-install-recommends git cmake make && \
# install library QT5.7 PPA from https://launchpad.net/~beineri/+archive/ubuntu/opt-qt57-xenial
# install add-apt-repository
  apt-get install -y --no-install-recommends software-properties-common && \
  add-apt-repository ppa:beineri/opt-qt57-xenial && apt-get update && \
# and install QT & OpenGL support
  apt-get install -y --no-install-recommends qt57declarative libgl1-mesa-dev && \
  /opt/qt57/bin/qt57-env.sh && apt-get purge -y software-properties-common --auto-remove

# add QT to path and set variable for .QT_OVERIDE_PATH 
ENV PATH=/opt/qt57/bin:$PATH QT5_PATH="/opt/qt57/bin"

# create user under which to run the tests
RUN useradd --uid 1000 -G sudo --create-home tester && mkdir /home/tester/bin && chown -R tester:tester /home/tester/bin
ADD bin /home/tester/bin
ENV PATH=/home/tester/bin:$PATH

# installing ccache
RUN apt-get install -y --no-install-recommends ccache
ENV PATH=/usr/lib/ccache:$PATH

WORKDIR /home/tester/opt

# installing Google Test & Mock
ADD googletest-1.8.0.zip /home/tester/opt/googletest-1.8.0.zip
# install dependencies
RUN apt-get update && apt-get purge -y python3 --auto-remove && apt-get install -y --no-install-recommends python-dev unzip && \
  unzip googletest-1.8.0.zip -x "*/docs/*" -x "*/msvc/*" -x "*/samples/*" && \
  rm googletest-1.8.0.zip && mv googletest-release-1.8.0 gtest && \
  apt-get purge -y --auto-remove unzip 

ENV GTEST_ROOT=/home/tester/opt/gtest/googletest
ENV GTEST_DIR=${GTEST_ROOT} GMOCK_DIR=/home/tester/opt/gtest/googlemock \
  GTEST_INCLUDE_DIR=${GTEST_ROOT}/include GMOCK_INCLUDE_DIR=/home/tester/opt/gtest/googlemock/include \
  GTEST_MAIN_LIBRARY=/home/tester/opt/gtest/libgmock.a GTEST_LIBRARY=/home/tester/opt/gtest/libgmock.a

# Install g++, compile GMock and GTest
# Version 1.7.0 expect library in GTEST_ROOT
RUN apt-get install -y --no-install-recommends g++ && \
  g++ -isystem ${GTEST_DIR}/include -I${GTEST_DIR} \
    -isystem ${GMOCK_DIR}/include -I${GMOCK_DIR} \
    -pthread -c ${GTEST_DIR}/src/gtest-all.cc && \
  g++ -isystem ${GTEST_DIR}/include -I${GTEST_DIR} \
   -isystem ${GMOCK_DIR}/include -I${GMOCK_DIR} \
   -pthread -c ${GMOCK_DIR}/src/gmock-all.cc && \
  ar -rv /home/tester/opt/gtest/libgmock.a gtest-all.o gmock-all.o && \
  cp /home/tester/opt/gtest/libgmock.a ${GMOCK_DIR}/ && \
  rm -rf /home/tester/opt/*/*.o

# Patch GMock
RUN apt-get install -y patch
ADD gmock_tokenize_public_slots.patch /home/tester/opt/gtest/googlemock/scripts/generator/cpp/tokenize.patch
WORKDIR /home/tester/opt/gtest/googlemock/scripts/generator/cpp
RUN patch -p1 < ./tokenize.patch

RUN apt-get install -y --no-install-recommends libeigen3-dev 

# temp utils, comment after tests are done
#RUN apt-get install -y --no-install-recommends ncdu apt-rdepends apt-file && apt-file update
#RUN apt-get install -y --no-install-recommends ncdu apt-rdepends

# cleanup
# remove caches, documents, logs and examples
# remove build temp files & unused packages
# remove apt-(get/cache) package list
RUN find /opt \( -name "doc" -o -name "examples" \) -type d -print0 | xargs -0 --no-run-if-empty rm -rf && \
  find /usr/share \( -name doc -o -name fonts -o -name "[hH]elp" -o -name man -o -name "[lL]ocale" \) -type d -print0 | xargs -0 --no-run-if-empty rm -rf && \
  find /var/log -name "*" -type f -print0 | xargs -0 --no-run-if-empty rm -f -- && \
  find /var/cache -name "*" -type f -print0 | xargs -0 --no-run-if-empty rm -f -- && \
  rm -R /root/.ccache && \
  apt-get purge -y locales --auto-remove && apt-get clean && apt-get -y autoclean && apt-get -y autoremove && \
  rm -rf /var/lib/apt/lists/* && rm -rf /var/lib/dpkg/info/*

ENV GCC_COLORS='error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01'

USER tester

# Configuring ccache in user account
RUN ccache --set-config=cache_dir=/cache --max-size=10G

WORKDIR /dublin

