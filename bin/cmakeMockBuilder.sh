#!/bin/bash
#Find all used mocks
SOURCES_DIR=$1
SOURCE_FILES=`find $SOURCES_DIR -name *.cpp`
HEADERS_FILES=`find $SOURCES_DIR -name *.h*`

RegularIFS=$IFS
NewLineIFS=$(echo -en "\n\b")

for sourceFile in $SOURCE_FILES
do
    dirName=`dirname $sourceFile`
    
    IFS=$NewLineIFS
	for newMock in $(grep "#include.*Mock.h" $sourceFile)
    do
	    newMock=`echo $newMock  | sed -e 's/#include..//g' | sed -e 's/.h.\$//g'`
    	#generate mock files
	    INTERFACE_TO_MOCK=`MockNameToDeduceClassName=${newMock} && echo "${MockNameToDeduceClassName%Mock}.h"`
    	echo "INTERFACE_TO_MOCK = $INTERFACE_TO_MOCK"

        IFS=$RegularIFS
    	for header in $HEADERS_FILES
	    do
	        if [ `basename $INTERFACE_TO_MOCK` == `basename $header` ]; then
	    	#create the mock for the first time
	            mmock $header ${dirName} "$newMock.h"
    	    fi
	    done

	done
    IFS=$RegularIFS
done

